'use strict';

var fs        = require('path'),
    crypto    = require('crypto'),
    path      = require('path');

/**
 * Check if file type is valid
 * @param  {Array} arr        Array of file types
 * @param  {String} extname   File type
 * @return {Boolean}
 */
exports.contains = function (list, val) {
  var contain = false;

  list.forEach(function (item) {
    if (val === item) {
      contain = true;
      return false;
    }
    return true;
  });
  return contain;
};

/**
 * Check if image exists
 * @param  {String} src
 * @return {Boolean}
 */
exports.imageExists = function (src) {
  return fs.existsSync(src);
};

/**
 * Get image directory
 * @param  {String} src
 * @return {String}
 */
exports.getImage = function (src) {
  return require(src);
};

exports.getImageExt = function (src) {
  return path.extname(src).slice(1);
};

exports.hashTmpName = function (src) {
  return crypto.createHash('md5').update(src).digest('hex');
};

