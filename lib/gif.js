/**
 * GIF commands
 */
'use strict';

var GifSicle  = require('gifsicle-stream');

exports.optimize = function (opts, callback) {
  console.log('gif.optimize()');

  var args = ['-w', '-O3'],
      arg;

  if (opts.interlaced) {
    args.push('-interlace');
  }

  var gifoptimize = new GifSicle(args);

  callback(null, gifoptimize);
};
