/**
 * PNG commands
 */
'use strict';

var OptiPng   = require('optipng'),
    PngQuant  = require('pngquant'),
    async     = require('async');

exports.optimize = function (opts, callback) {
  console.log('png.optimize()');
  opts = opts || {};

  var pngoptimize, arg;
  if (typeof opts.optLevel === 'undefined') {
    pngoptimize = new OptiPng(['-o7']);
  } else {
    arg = '-o' + opts.optLevel;
    pngoptimize = new OptiPng([arg]);
  }

  /*if (typeof opts.binary === 'undefined') {
    opts.binary = 256;
    if (!opts.ordered) {
      cmds.quant = new PngQuant([opts.binary]);
    } else {
      cmds.quant = new PngQuant([opts.binary, '-ordered']);
    }
  } else {
    if (!opts.ordered) {
      cmds.quant = new PngQuant([opts.binary]);
    } else {
      cmds.quant = new PngQuant([opts.binary, '-ordered']);
    }
  }*/

  callback(null, pngoptimize);
};
