/**
 * JPG commands
 */
'use strict';

var JpegTran      = require('jpegtran');

exports.crop = function (w, h, x, y, callback) {
  console.log('jpg.crop()');

  if (typeof w === 'undefined') {
    w = 1024;
  }

  if (typeof h === 'undefined') {
    h = 1024;
  }
  var args = ['-crop'],
      d = w + 'x' + h + '+' + (x || 0) + '+' + (y || 0);
  args.push(d);

  var jpgcrop = new JpegTran(args);

  callback(null, jpgcrop);
};

exports.rotate = function (deg, callback) {
  console.log('jpg.rotate()');

  var args = ['-rotate', '-progressive'];
  args[1] = deg;

  var jpgrotate = new JpegTran(args);

  callback(null, jpgrotate);
};

exports.scale = function (w, h, callback) {
  console.log('jpg.scale()');
  var args = ['-scale'],
      dimensions;

  if (w && h) {
    dimensions = m + '/' + n;
    args.push(dimensions);
  } else if (w && !h) {
    dimensions = '';
    args.push(dimensions);
    callback(new Error('Please provide height!'));
  } else if (!w && h) {
    dimensions = '';
    args.push(dimensions);
    callback(new Error('Please provide width!'));
  } else {
    dimensions = 800 + 'x' + 800;
    args.push(dimensions);
  }

  var jpgscale = new JpegTran(args);

  callback(null, jpgscale);
};

exports.resize = function (w, h, callback) {
  console.log('jpg.resize()');

  var wIsValid = Boolean(w || w === 0),
      hIsValid = Boolean(h || h === 0),
      args = ['-resize'],
      dimensions;

  if (wIsValid && hIsValid) {
    dimensions = w + 'x' + h;
    args.push(dimensions);
  } else if (wIsValid) {
    h = w;
    dimensions = w + 'x' + h;
    args.push(dimensions);
  } else if (hIsValid) {
    w = h;
    dimensions = w + 'x' + h;
    args.push(dimensions);
  } else {
    w = 800;
    h = 800;
    dimensions = w + 'x' + h;
    args.push(dimensions);
  }

  var jpgresize = new JpegTran(args);

  callback(null, jpgresize);
};

exports.minify = function (callback) {
  console.log('jpg.minify()');

  var jpgminify = new JpegTran(['-minify']);

  callback(null, jpgminify);
};

exports.optimize = function (opts, callback) {
  console.log('jpg.optimize()');
  opts = opts || {};

  var args = ['-copy', 'none', '-optimize'];

  if (opts.progressive) {
    args.push('-progressive');
  }

  if (opts.arithmetic) {
    args.push('-arithmetic');
  }

  var jpgoptimize = new JpegTran(args);

  callback(null, jpgoptimize);
};
