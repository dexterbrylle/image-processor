'use strict';

var Stream            = require('stream').Stream,
    util              = require('util'),
    fs                = require('fs'),
    gm                = require('gm'),
    jpg               = require('./lib/jpg'),
    png               = require('./lib/png'),
    gif               = require('./lib/gif'),
    utils             = require('./lib/utils'),
    sizeOf            = require('image-size'),
    arrJpg            = ['jpg', 'jpeg'],
    arrPng            = ['png', 'tiff'],
    arrGif            = ['gif'];

//DIRTY CODE -- needs refactoring
function ImageProcessor(src) {
  Stream.call(this);

  this.writable = this.readable = true;
  this.hasEnded = false;

  this.src = src;
}

util.inherits(ImageProcessor, Stream);

ImageProcessor.prototype._transform = function (data, encoding, callback) {
  this.push(data);
  callback();
};

ImageProcessor.prototype.hasError = function (err) {
  if (!this.hasEnded) {
    this.hasEnded = true;
    this.emit('error', err);
  }
};

ImageProcessor.prototype.crop = function (w, h, x, y, callback) {
  var source = this.src, file, ws;

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = source;
    file.pause();
  }

  file.resume();

  if (utils.contains(arrJpg, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-cropped.jpg');
    jpg.crop(w, h, x, y, function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('end', function () {
        console.log('Done reading!');
      });
      file.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrPng, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photos/photo-cropped.png');
    // file.resume();
    gm(file)
      .crop(w, h, x, y)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else if (utils.contains(arrGif, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-cropped.gif');
    gm(file)
      .crop(w, h, x, y)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else {
    callback(new Error('I need a file!'));
  }

};

ImageProcessor.prototype.rotate = function (deg, callback) {
  var source = this.src,
      file, ws;

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = source;
    file.pause();
  }

  file.resume();

  if (utils.contains(arrJpg, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-rotated.jpg');
    jpg.rotate(deg, function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('error', function (err) {
        console.error(err);
      });
      file.on('end', function () {
        console.log('Done reading');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrPng, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-rotated.png');
    gm(file)
      .rotate(opts)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else if (utils.contains(arrGif, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-rotated.gif');
    gm(file)
      .rotate(opts)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else {
    callback(new Error('I need a file!'));
  }

};

ImageProcessor.prototype.scale = function (w, h, callback) {
  var source = this.src,
      file, ws;

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = source;
    file.pause();
  }

  file.resume();

  if (utils.contains(arrJpg, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-scaled.jpg');
    jpg.scale(w, h, function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('error', function (err) {
        console.error(err);
      });
      file.on('end', function () {
        console.log('Done reading');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrPng, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-scaled.png');
    gm(file)
      .scale(w, h)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else if (utils.contains(arrGif, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-scaled.gif');
    gm(file)
      .scale(w, h)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else {
    callback(new Error('I need a file!'));
  }
};

ImageProcessor.prototype.resize = function (w, h, callback) {
  var source = this.src,
      file, ws;

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = source;
    file.pause();
  }

  file.resume();

  if (utils.contains(arrJpg, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-resized.jpg');
    jpg.resize(w, h, function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('error', function (err) {
        console.error(err);
      });
      file.on('end', function () {
        console.log('Done reading');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrPng, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-resized.png');
    gm(file)
      .resize(w, h)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });

  } else if (utils.contains(arrGif, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-resized.gif');
    gm(file)
      .resize(w, h)
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else {
    callback(new Error('I need a file!'));
  }
};

ImageProcessor.prototype.minify = function (callback) {
  var source = this.src,
      file, ws;

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = source;
    file.pause();
  }

  file.resume();

  if (utils.contains(arrJpg, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-minified.jpg');
    jpg.minify(function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('error', function (err) {
        console.error(err);
      });
      file.on('end', function () {
        console.log('Done reading');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrPng, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-minified.png');
    gm(file)
      .minify()
      .stream(function (err, stdout, stderr) {
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else if (utils.contains(arrGif, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-minified.gif');
    gm(file)
      .minify()
      .stream(function (err, stdout, stderr){
        stdout.pipe(ws);
        callback(null, ws);
      });
    file.on('error', function (err) {
      console.error(err);
    });
    file.on('end', function () {
      console.log('Done reading');
    });
    ws.on('error', function (err) {
      console.error(err);
    });
    ws.on('end', function () {
      console.log('Done writing!');
    });
  } else {
    callback(new Error('I need a file!'));
  }
};

ImageProcessor.prototype.optimize = function (opts, callback) {
  var source = this.src,
      file, ws;

  opts = this.opts || {};

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = source;
    file.pause();
  }
  file.resume();

  if (utils.contains(arrJpg, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-optimized.jpg');
    jpg.optimize(opts, function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('end', function () {
        console.log('Done reading!');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrPng, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-optimized.png');
    png.optimize(opts, function (err, data) {
      if (err) {
        console.log(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('end', function () {
        console.log('Done reading');
      });
      file.on('error', function (err) {
        console.error(err);
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      callback(null, ws);
    });
  } else if (utils.contains(arrGif, utils.getImageExt(file.path))) {
    ws = fs.createWriteStream(__dirname + '/photo-optimized.gif');
    gif.optimize(opts, function (err, data) {
      if (err) {
        console.error(err);
        callback(err);
        return;
      }
      file.pipe(data).pipe(ws);
      file.on('end', function () {
        console.log('Done reading!');
      });
      file.on('error', function (err) {
        console.error(err);
      });
      ws.on('end', function () {
        console.log('Done writing!');
      });
      ws.on('error', function (err) {
        console.error(err);
      });
      callback(null, ws);
    });
  } else {
    callback(new Error('Pass a valid file!'));
  }
};

ImageProcessor.prototype.getImgDimensions = function (callback) {
  var source = this.src,
      dimensions, file;

  if (typeof source === 'string') {
    file = fs.createReadStream(source);
    file.pause();
  } else if (source instanceof Stream) {
    file = this.src;
    file.pause();
  }
  file.resume();
  dimensions = sizeOf(file.path);
  callback(null, dimensions);
};

module.exports = ImageProcessor;
