# Image Processor using Node Streams
------------------------------------

This library processes images by passing a readabale stream (or file paths) and writes it into a writable stream. It uses [jpegtran](https://www.npmjs.com/package/jpegtran) for jpg methods, [optipng](https://www.npmjs.com/package/optipng) and [gifsicle-stream](https://www.npmjs.com/package/gifsicle-stream) for optimizing png & gif files, respectively. The current fallback for PNG and GIF operations uses [gm](https://www.npmjs.com/package/gm).

------------------------------------

### Installation & Usage

```
npm install --save image-processor
```

```javascript
var ImageProcessor = require('image-processor'),
    rs = fs.createReadStream('photos/batman.gif'),
    photo = 'photos/photo2.jpg';

// Accept a readable stream
var imageProcessor = new ImageProcessor(rs);

// Accept a file path
var imageProcessorFilePath = new ImageProcessor(photo);

//Configure options
//See API below to properly configure options per method
var opts = { interlaced: true, arithmetic: true };

imageProcessor.optimize(opts, function (err, data) {
  if (err) {
    console.error(err);
  }

  res.send(data);
});
```

-------------------------------------

### API

  - __imageProcessor.crop(w, h, x, y, callback);__
    ```javascript
      imageStream.crop(800, 800, 0, 0, function (err, data) {
        if (err) { console.log(err); }

        console.log(data);
      });
    ```

  - __imageProcessor.rotate(deg, callback);__
    ```javascript
      imageStream.rotate(270, function (err, data) {
        if (err) { console.log(err); }

        console.log(data);
      });
    ```
  - __imageProcessor.scale(w, h, callback);__
    ```javascript
      imageStream.scale(1, 8, function (err, data) {
        if (err) { console.log(err); }

        console.log(data);
      });
    ```

  - __imageProcessor.resize(w, h, callback);__
    ```javascript
      imageStream.resize(800, 800, function (err, data) {
        if (err) { console.log(err); }

        console.log(data);
      });
    ```
  - __imageProcessor.minify(callback);__
    ```javascript
      imageStream.minify(function (err, data) {
        if (err) { console.log(err); }

        console.log(data);
      });
    ```
  - __imageProcessor.optimize(opts, callback);__
    ```javascript
      var opts = {
        progressive: true,
        arithmetic: true
      };
      imageStream.optimize(opts, function (err, data) {
        if (err) { console.log(err); }

        console.log(data);
      });
    ```
  - __imageProcessor.getImgDimensions(callback);__
    ```javascript
      imageStream.getImgDimensions(function(err, dimensions) {
        if (err) { console.log(err); }

        console.log('width: ' + dimensions.width + ' x ' + 'height: ' + dimensions.height);
      });
    ```

-------------------------------------

### Supported file types

 * __JPG__ / __JPEG__
 * __PNG__
 * __GIF__
 * __TIFF__

 -------------------------------------
